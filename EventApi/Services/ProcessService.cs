﻿using System;
using System.Linq;
using System.Threading.Tasks;
using EventApi.Context;
using EventApi.Hubs;
using EventDomain.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using StackExchange.Redis;

namespace EventApi.Services
{
    public class ProcessService : IProcessService
    {
        private readonly EventContext _eventContext;
        private readonly IBatchHubService _batchHubService;

        public ProcessService(EventContext eventContext, IBatchHubService batchHubService)
        {
            _eventContext = eventContext;
            _batchHubService = batchHubService;
        }

        public async Task HandleProcessComplete(int processId)
        {
            var process = _eventContext.Processes.First(p => p.Id == processId);
            var batch = _eventContext.Batches.Include(b => b.Processes).First(b => b.Id == process.BatchId);
            var batchPending = batch.Processes.Any(p => p.CompletedDate == null);
            if (!batchPending)
            {
                batch.CompletedDate = DateTime.Now;
                _eventContext.Batches.Update(batch);
                await _eventContext.SaveChangesAsync();
                
            }
            await _batchHubService.SendBatchUpdateMessage(batch.Id);
            
        }

        
    }
}
