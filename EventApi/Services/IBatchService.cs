﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EventDomain.Models;

namespace EventApi.Services
{
    public interface IBatchService
    {
        Task<Batch> GenerateBatch(List<ProcessType> processTypes);
    }
}