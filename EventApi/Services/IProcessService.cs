﻿using System.Threading.Tasks;

namespace EventApi.Services
{
    public interface IProcessService
    {
        Task HandleProcessComplete(int processId);
    }
}