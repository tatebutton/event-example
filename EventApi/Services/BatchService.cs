﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EventApi.Context;
using EventApi.Extensions;
using EventApi.Hubs;
using EventApi.Messaging;
using EventApi.Models;
using EventDomain.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace EventApi.Services
{
    public class BatchService : IBatchService
    {
        private readonly EventContext _eventContext;
        private readonly MessagingClient _messagingClient;
        private readonly IBatchHubService _batchHubService;
        public BatchService(EventContext eventContext, MessagingClient messagingClient, IBatchHubService batchHubService)
        {
            _eventContext = eventContext;
            _messagingClient = messagingClient;
            _batchHubService = batchHubService;
        }



        public async Task<Batch> GenerateBatch(List<ProcessType> processTypes)
        {
            var batch = await CreateBatch(processTypes);
            if (batch != null)
            {
                await _messagingClient.SendBatchMessage(batch.Id);
                await _batchHubService.SendBatchUpdateMessage(batch.Id);
            }
            return batch;

        }

        private async Task<Batch> CreateBatch(List<ProcessType> processTypes)
        {
            if (processTypes.Any())
            {
                var batch = new Batch()
                {
                    InitializedDate = DateTime.Now,
                    Processes = processTypes.Select(pt => new Process().ByType(pt)).ToList()
                };
                await _eventContext.Batches.AddAsync(batch);
                await _eventContext.SaveChangesAsync();
                return batch;

            }

            throw new ApplicationException("No processes defined for batch. Could not create batch");

        }
        
    }
}
