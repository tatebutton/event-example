﻿namespace EventApi
{
    public class AppSettings
    {
        public string ServiceBusConnectionString { get; set; }
        public string BatchTopicName { get; set; }
        public string ProcessTopicName { get; set; }
        public string ApiProcessTopicSubscriptionName { get; set; }
        public string UnoBatchTopicSubscriptionName { get; set; }
        public string DosBatchTopicSubscriptionName { get; set; }
    }
}
