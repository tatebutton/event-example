﻿using System;
using EventDomain.Models;

namespace EventApi.Extensions
{
    public static class ProcessExtensions
    {
        public static Process ByType(this Process process, ProcessType type)
        {
            process.ProcessType = type;
            process.InitializedDate = DateTime.UtcNow;
            return process;

        }
    }
}
