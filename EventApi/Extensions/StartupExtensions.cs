﻿using System;
using System.Data.SqlClient;

namespace EventApi.Extensions
{
    public static class StartupExtensions
    {
        public static bool DbConnectionValid(string connection)
        {
            using (SqlConnection conn = new SqlConnection(connection))
            {
                try
                {
                    conn.Open();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
}
