﻿using System;
using System.Data.SqlClient;
using AutoMapper;
using EventApi.Context;
using EventApi.Extensions;
using EventApi.Hubs;
using EventApi.Messaging;
using EventApi.Services;
using Hangfire;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace EventApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
           

            services.AddMvc();
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
            services.Configure<PubSubSettings>(Configuration.GetSection("PubSubSettings"));
            services.AddDbContext<EventContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));

            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
            });
            services.AddHangfire(options =>
                {
                    options.UseSqlServerStorage(Configuration.GetConnectionString("DefaultConnection"),
                        new SqlServerStorageOptions());
                });



            services.AddAutoMapper(typeof(Startup));

            
            services.AddScoped<IBatchService, BatchService>();
            services.AddScoped<IProcessService, ProcessService>();
            services.AddScoped<IBatchHubService, BatchHubService>();
            services.AddSingleton<MessagingClient>();
            
            
            
            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseCors(builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials();
            });
            
            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
            
            if (StartupExtensions.DbConnectionValid(Configuration.GetConnectionString("DefaultConnection")))

            {
                var options = new BackgroundJobServerOptions
                {
                    ServerName = "WebServer"
                };

                app.UseHangfireServer(options);
                app.UseHangfireDashboard();
            }
            
            
            
            app.ApplicationServices.GetRequiredService<MessagingClient>().Initialize();

            

            app.UseSignalR(routes => { routes.MapHub<BatchHub>("/hubs/batch"); });
        }


    }
}
