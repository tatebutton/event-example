﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventDomain.Models;

namespace EventApi.Models.ResponseModels
{
    public class ProcessResponseModel
    {
        public int ProcessId { get; set; }
        public DateTime? InitializedDate { get; set; }
        public DateTime? CompletedDate { get; set; }
        public ProcessType ProcessType { get; set; }
        public int BatchId { get; set; }
    }
}
