﻿using System.Collections.Generic;
using EventDomain.Models;

namespace EventApi.Models.PostModels
{
    public class BatchPostModel
    {
        public List<ProcessType> ProcessTypes { get; set; }
    }
}
