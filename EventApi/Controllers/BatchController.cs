﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EventApi.Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EventApi.Models.PostModels;
using EventApi.Services;
using EventDomain.Models;

namespace EventApi.Controllers
{
    [Route("api/Batches")]
    public class BatchesController : Controller
    {
        private readonly EventContext _context;
        private readonly IBatchService _batchService;

        public BatchesController(EventContext context, IBatchService batchService)
        {
            _context = context;
            _batchService = batchService;
        }

        // GET: api/Batches
        [HttpGet]
        public IEnumerable<Batch> GetBatches()
        {
            return _context.Batches;
        }

        // GET: api/Batches/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBatch([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var batch = await _context.Batches.SingleOrDefaultAsync(m => m.Id == id);

            if (batch == null)
            {
                return NotFound();
            }

            return Ok(batch);
        }

        // PUT: api/Batches/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBatch([FromRoute] int id, [FromBody] Batch batch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != batch.Id)
            {
                return BadRequest();
            }

            _context.Entry(batch).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BatchExists(id))
                {
                    return NotFound();
                }

                throw;
            }

            return NoContent();
        }

        // POST: api/Batches
        [HttpPost]
        public async Task<IActionResult> PostBatch([FromBody] BatchPostModel batchPost)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var batch = await _batchService.GenerateBatch(batchPost.ProcessTypes);
                return Ok(batch);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // DELETE: api/Batches/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBatch([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var batch = await _context.Batches.SingleOrDefaultAsync(m => m.Id == id);
            if (batch == null)
            {
                return NotFound();
            }

            _context.Batches.Remove(batch);
            await _context.SaveChangesAsync();

            return Ok(batch);
        }

        private bool BatchExists(int id)
        {
            return _context.Batches.Any(e => e.Id == id);
        }
    }
}