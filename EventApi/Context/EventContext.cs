﻿using EventDomain.Models;
using Microsoft.EntityFrameworkCore;

namespace EventApi.Context
{
    public class EventContext : DbContext
    {
        public EventContext(DbContextOptions<EventContext> options) 
            : base(options)
        {

        }
        public DbSet<Batch> Batches { get; set; }
        public DbSet<Process> Processes { get; set; }
    }
}
