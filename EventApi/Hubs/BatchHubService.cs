﻿using System.Linq;
using System.Threading.Tasks;
using EventApi.Context;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;

namespace EventApi.Hubs
{
    public interface IBatchHubService
    {
        Task SendBatchUpdateMessage(int batchId);
    }

    public class BatchHubService : IBatchHubService
    {
        private readonly IHubContext<BatchHub> _batchHub;
        private readonly EventContext _eventContext;

        public BatchHubService(IHubContext<BatchHub> batchHub, EventContext eventContext)
        {
            _batchHub = batchHub;
            _eventContext = eventContext;
        }
        
        public async Task SendBatchUpdateMessage(int batchId)
        {
            var batch = _eventContext.Batches.Include(b => b.Processes).First(b => b.Id == batchId);
            await _batchHub.Clients.All.SendAsync("BatchUpdate",batch);
        }
    }
}