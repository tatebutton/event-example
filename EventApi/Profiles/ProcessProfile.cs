﻿using AutoMapper;
using EventApi.Models.ResponseModels;
using EventDomain.Models;

namespace EventApi.Profiles
{
    public class ProcessProfile : Profile
    {
        public ProcessProfile()
        {
            CreateMap<Process, ProcessResponseModel>();
        }
    }
}
