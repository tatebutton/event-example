﻿using AutoMapper;
using EventApi.Models.ResponseModels;
using EventDomain.Models;

namespace EventApi.Profiles
{
    public class BatchProfile : Profile
    {
        public BatchProfile()
        {
            CreateMap<Batch, BatchResponseModel>();
        }
        
    }
}
