﻿using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EventApi.Services;
using Google.Cloud.PubSub.V1;
using Hangfire;
using Newtonsoft.Json;

namespace EventApi.Messaging
{
    public class MessagingClient
    {
        private readonly  PublisherClient _batchTopicSenderClient;
        private readonly SubscriberClient _processTopicReceiverClient;
        //private readonly EventContext _eventContext;

        public MessagingClient()
        {

            var publisherClient = PublisherServiceApiClient.Create();
            _batchTopicSenderClient = PublisherClient.Create(new TopicName("hy-vee-event-poc", "batch"), new [] {publisherClient});

            
            var subscriptionName = new SubscriptionName("hy-vee-event-poc", "apiSubscriber");
            var subscriberServiceApiClient = SubscriberServiceApiClient.Create();
            _processTopicReceiverClient = SubscriberClient.Create(subscriptionName, new[] {subscriberServiceApiClient});
            

        }

        public void Initialize()
        {
            InitializeReceiver(_processTopicReceiverClient);
        }

        public  async Task SendBatchMessage(int batchId)
        {
            await _batchTopicSenderClient.PublishAsync(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(batchId)));
        }

        private  void InitializeReceiver(SubscriberClient subscriberClient)
        {
            subscriberClient.StartAsync((msg, token) =>
            {
                
                    if (msg != null)
                    {
                        var processId = int.Parse(Encoding.UTF8.GetString(msg.Data.ToByteArray()));
                        BackgroundJob.Enqueue<IProcessService>(ps => ps.HandleProcessComplete(processId));
                   
                    }
                    return Task.FromResult(SubscriberClient.Reply.Ack);
                
            });
            
        }


    }
}
