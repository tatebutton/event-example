﻿using System;
using System.ComponentModel;
using System.IO;
using EventApi.Context;
using EventApi.Hubs;
using EventApi.Services;
using Hangfire;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace HangFireService
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            IConfiguration configuration = builder.Build();

            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection, configuration);

            // create service provider
            var serviceProvider = serviceCollection.BuildServiceProvider();
            
            GlobalConfiguration.Configuration.UseActivator(new HangfireActivator(serviceProvider));
            GlobalConfiguration.Configuration.UseSqlServerStorage(configuration["ConnectionStrings:DefaultConnection"]);

            
            using (var server = new BackgroundJobServer())
            {
                Console.WriteLine("Hangfire Server started. Press any key to exit...");

                Console.ReadKey();
            }
        }

        private static void ConfigureServices(IServiceCollection serviceCollection, IConfiguration configuration)
        {
            //db context
            serviceCollection.AddDbContext<EventContext>(options =>
            {
                options.UseSqlServer(configuration["ConnectionStrings:DefaultConnection"]);

            });

            // add services
            serviceCollection.AddScoped<IBatchService, BatchService>();
            serviceCollection.AddScoped<IProcessService, ProcessService>();
            serviceCollection.AddDbContext<EventContext>();

        }
    }
}
