﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DapperDo;
using EventDomain.Models;
using Google.Cloud.PubSub.V1;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace UnoService
{
    class Program
    {
        public static PublisherClient ProcessTopicSenderClient;
        public static SubscriberClient BatchTopicReceiverClient;
        public static IConfiguration Configuration;
        public async Task Run(IConfigurationRoot configuration)
        {
            Configuration = configuration;
            var publisherClient = PublisherServiceApiClient.Create();
            ProcessTopicSenderClient = PublisherClient.Create(new TopicName("hy-vee-event-poc", "process"), new [] {publisherClient});

            
            var subscriptionName = new SubscriptionName("hy-vee-event-poc", "unoSubscriber");
            var subscriberServiceApiClient = SubscriberServiceApiClient.Create();
            BatchTopicReceiverClient = SubscriberClient.Create(subscriptionName, new[] {subscriberServiceApiClient});
            
            
            
            
            InitializeReceiver(BatchTopicReceiverClient, ConsoleColor.Cyan);

            lock (Console.Out)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Uno service started");
                Console.ResetColor();

            }


            await Task.WhenAny(
                Task.Run(() => Console.ReadKey())
            );

            await BatchTopicReceiverClient.StopAsync(CancellationToken.None);
        }

        public static async Task SendProcessCompleteMessage(int processId)
        {
            await ProcessTopicSenderClient.PublishAsync(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(processId)));
        }

       void InitializeReceiver(SubscriberClient receiver, ConsoleColor color)
       {
          
            
            receiver.StartAsync(async (msg, token) =>
            {
                Console.WriteLine("Uno Message: " + msg);
                if (msg != null)
                {
                    var dd = new DapperDoer(Configuration["ConnectionStrings:DefaultConnection"]);
                    var batchId = int.Parse(Encoding.UTF8.GetString(msg.Data.ToByteArray()));
                    Console.WriteLine("BatchID: " + batchId);
                    
                    var process = (await dd.GetProcessesByBatchIdAndType(batchId, ProcessType.Uno)).FirstOrDefault();

                    if (process != null)
                    {
                        await dd.InitializeProcess(process.Id);

                        var random = new Random().Next(1, 2000);
                        Console.WriteLine($"sleep {random} ms");
                        Thread.Sleep(random);

                        await dd.CompleteProcess(process.Id);
                        await SendProcessCompleteMessage(process.Id);
                        
                        Console.WriteLine($"Service finished processing for batch {batchId}");
                    }
                    
                   
                }
                return await Task.FromResult(SubscriberClient.Reply.Ack);
                
            });
    
        }

       

        public static int Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            IConfigurationRoot configuration = builder.Build();

            try
            {
                var app = new Program();
                app.Run(configuration).GetAwaiter().GetResult();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return 1;
            }
            return 0;
        }

    }
}
