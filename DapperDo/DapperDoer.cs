﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;
using EventDomain.Models;
using Process = EventDomain.Models.Process;

namespace DapperDo
{
    public class DapperDoer
    {
        private readonly SqlConnection _connection;
        public DapperDoer(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
        }

        public async Task<IEnumerable<Process>> GetProcessesByBatchIdAndType(int batchId, ProcessType processType)
        {
            
//                var tm1 = new Stopwatch();
//                tm1.Start();
//                var a = (await _connection.GetAllAsync<Process>()).Where(p => p.BatchId == batchId && p.ProcessType == processType);
//                tm1.Stop();
            try
            {
                var tm2 = new Stopwatch();
                tm2.Start();
                var rows = await _connection.QueryAsync<Process>(
                    $"select * from process p where p.batchid = {batchId} and p.processtype = {(int)processType}");
                tm2.Stop();

//                Console.WriteLine($"    dapper contrib get= {tm1.Elapsed.TotalMilliseconds} ms");
                Console.WriteLine($"    dapper get= {tm2.Elapsed.TotalMilliseconds} ms");
                return rows;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Dapper Get Proccess {e}");
                throw;
            }
               
            
        }

        public async Task CompleteProcess(int processId)
        {
            try
            {
                await _connection.ExecuteAsync(
                    $"UPDATE process set completeddate = '{DateTime.UtcNow:yyyy-MM-dd HH:mm:ss.fffffff}' where id = {processId}");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Dapper Complete Proccess {e}");
                throw;
            }
       
        }

        public async Task InitializeProcess(int processId)
        {
            try
            {
                await _connection.ExecuteAsync(
                    $"UPDATE process set InitializedDate = '{DateTime.UtcNow:yyyy-MM-dd HH:mm:ss.fffffff}' where id = {processId}");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Dapper Initialize Proccess {e}");
                throw;
            }
            

        }






    }
}
