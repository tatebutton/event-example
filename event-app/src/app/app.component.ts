import { Component, OnInit } from '@angular/core';
import { HubConnection } from '@aspnet/signalr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'event driven design example';

  private _hubConnection: HubConnection;

  public batches: any[] = [];



  ngOnInit(): void {
    this._hubConnection = new HubConnection('http://localhost:5000/hubs/batch');

    this._hubConnection.on('BatchUpdate', (batch: any) => {
      console.log('received batch', batch);
      this.handleBatchUpdate(batch);
    });

    this._hubConnection.start()
            .then(() => {
                console.log('Hub connection started')
            })
            .catch(() => {
                console.log('Error while establishing connection');
            });
  }

  handleBatchUpdate(newBatch: any) {
    let batchIndex = this.batches.findIndex(item => item.id == newBatch.id);
    if(batchIndex != -1){
      this.batches[batchIndex] = newBatch;
    }
    else{
      this.batches.push(newBatch);
    }
    
  }

}
