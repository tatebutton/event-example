﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EventDomain.Models
{
    [Table("Batch")]
    public class Batch
    {
        [Key]
        public int Id { get; set; }
        public DateTime? InitializedDate { get; set; }
        public DateTime? CompletedDate { get; set; }
        public virtual ICollection<Process> Processes { get; set; }

    }
}