﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EventDomain.Models
{
    [Table("Process")]
    public class Process
    {
        [Key]
        public int Id { get; set; }
        public DateTime? InitializedDate { get; set; }
        public DateTime? CompletedDate { get; set; }
        public ProcessType ProcessType { get; set; } 
        public int BatchId { get; set; }

    }
}